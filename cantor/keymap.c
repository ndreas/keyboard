// Copyright 2022 Diego Palacios (@diepala)
// SPDX-License-Identifier: GPL-2.0

#include QMK_KEYBOARD_H

// !
#define ND_EXLM S(KC_1)
// "
#define ND_DQUO S(KC_2)
// @
#define ND_AT RALT(KC_2)
// #
#define ND_HASH S(KC_3)
// $
#define ND_DLR RALT(KC_4)
// %
#define ND_PERC S(KC_5)
// &
#define ND_AMPR S(KC_6)
// /
#define ND_SLSH S(KC_7)
// |
#define ND_PIPE RALT(KC_7)
// backslash
#define ND_BSLS RALT(S(KC_7))

// ( )
#define ND_LPRN S(KC_8)
#define ND_RPRN S(KC_9)
// [ ]
#define ND_LBRC RALT(KC_8)
#define ND_RBRC RALT(KC_9)
// { }
#define ND_LCBR RALT(S(KC_8))
#define ND_RCBR RALT(S(KC_9))
// < >
#define ND_LABK KC_NUBS
#define ND_RABK S(KC_NUBS)

// =
#define ND_EQL S(KC_0)
// +
#define ND_PLUS KC_MINS
// ?
#define ND_QUES S(KC_MINS)
// `
#define ND_GRV S(KC_EQL)
// ~
#define ND_TILD KC_RBRC
// ^
#define ND_CIRC S(KC_RBRC)
// '
#define ND_QUOT KC_NUHS
// *
#define ND_ASTR S(KC_NUHS)

#define KC_SYMR MO(_SYMR)
#define KC_SYML MO(_SYML)
#define KC_NAV MO(_NAV)
#define KC_MEDIA MO(_MEDIA)

/*
 * Thumb buttons (L = left, R = right)
 */
// tap-hold _NAV/ESC
#define ND_TL1 LT(_NAV,KC_ESC)
// tap-hold _SYMR/space
#define ND_TL2 LT(_SYMR,KC_SPC)
#define ND_TL3 KC_LGUI
// tap-hold _NUM/enter
#define ND_TR1 LT(_NUM,KC_ENT)
// tap-hold _SYML/backspace
#define ND_TR2 LT(_SYML,KC_BSPC)
#define ND_TR3 KC_RGUI

// tap-hold ctrl/f9
#define ND_CTF9 LCTL_T(KC_F9)

// mod+key
#define ND_GSPC G(KC_SPC)
#define ND_GENT G(KC_ENT)

enum cantor_layers {
    _ROOT,
    _SYML,
    _SYMR,
    _NUM,
    _NAV,
    _GUI
};

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
[_ROOT] = LAYOUT_split_3x6_3(
   KC_TAB,    KC_Q,    KC_W,    KC_E,    KC_R,    KC_T,                      KC_Y,    KC_U,    KC_I,    KC_O,    KC_P, KC_LBRC,
  ND_CTF9,    KC_A,    KC_S,    KC_D,    KC_F,    KC_G,                      KC_H,    KC_J,    KC_K,    KC_L, KC_SCLN, KC_QUOT,
  KC_LSFT,    KC_Z,    KC_X,    KC_C,    KC_V,    KC_B,                      KC_N,    KC_M, KC_COMM,  KC_DOT, KC_SLSH, KC_RSFT,
                                       ND_TL3,  ND_TL2,  ND_TL1,   ND_TR1, ND_TR2,  ND_TR3
)
,[_SYML] = LAYOUT_split_3x6_3(
  _______, XXXXXXX, ND_HASH,  ND_GRV,  ND_DLR, ND_PERC,                    _______, _______, _______, _______, _______, _______,
  _______,   ND_AT, ND_QUOT, ND_DQUO, ND_EXLM,  ND_EQL,                    _______, _______, _______, _______, _______, _______,
  ND_LABK, ND_RABK, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX,                    _______, _______, _______, _______, _______, _______,
                                      _______, _______, ND_GSPC,  _______, _______, _______
)
,[_SYMR] = LAYOUT_split_3x6_3(
  _______, _______, _______, _______, _______, _______,                    XXXXXXX, ND_PIPE, ND_LPRN, ND_RPRN, ND_PLUS, ND_CIRC,
  _______, _______, _______, _______, _______, _______,                    ND_AMPR, ND_SLSH, ND_LCBR, ND_RCBR, ND_TILD, ND_ASTR,
  _______, _______, _______, _______, _______, _______,                    XXXXXXX, ND_BSLS, ND_LBRC, ND_RBRC, ND_QUES, _______,
                                      _______, _______, _______,  ND_GENT, _______, _______
)
,[_NUM] = LAYOUT_split_3x6_3(
  _______, XXXXXXX,    KC_1,    KC_2,    KC_3, XXXXXXX,                      KC_F1,   KC_F2,   KC_F3,   KC_F4,   KC_F5,   KC_F6,
  _______,    KC_0,    KC_4,    KC_5,    KC_6, XXXXXXX,                    XXXXXXX, KC_RGUI, KC_RCTL, XXXXXXX, XXXXXXX, XXXXXXX,
  _______, XXXXXXX,    KC_7,    KC_8,    KC_9, XXXXXXX,                      KC_F7,   KC_F8,   KC_F9,  KC_F10,  KC_F11,  KC_F12,
                                      _______, _______, _______,  _______, _______, _______
)
,[_NAV] = LAYOUT_split_3x6_3(
   QK_RBT, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX,                    KC_WBAK, KC_PGUP, KC_HOME, KC_WFWD, XXXXXXX, XXXXXXX,
  XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX,                    KC_LEFT, KC_DOWN,   KC_UP, KC_RGHT, XXXXXXX, XXXXXXX,
  XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX,                    XXXXXXX, KC_PGDN,  KC_END, XXXXXXX, XXXXXXX, XXXXXXX,
                                      XXXXXXX, XXXXXXX, XXXXXXX,  XXXXXXX, XXXXXXX, XXXXXXX
)
,[_GUI] = LAYOUT_split_3x6_3(
  XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX,                    XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX,
  XXXXXXX, G(KC_1), G(KC_2), G(KC_3), G(KC_4), G(KC_5),                    G(KC_6), G(KC_7), G(KC_8), G(KC_9), G(KC_0), XXXXXXX,
  XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX,                    XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX,
                                      XXXXXXX, XXXXXXX, XXXXXXX,  XXXXXXX, XXXXXXX, XXXXXXX
)
/*
,[_] = LAYOUT_split_3x6_3(
  _______, _______, _______, _______, _______, _______,                    _______, _______, _______, _______, _______, _______,
  _______, _______, _______, _______, _______, _______,                    _______, _______, _______, _______, _______, _______,
  _______, _______, _______, _______, _______, _______,                    _______, _______, _______, _______, _______, _______,
                                      _______, _______, _______,  _______, _______, _______
)
*/
};

const uint16_t PROGMEM df_combo[] = {KC_D, KC_F, COMBO_END};
const uint16_t PROGMEM jk_combo[] = {KC_J, KC_K, COMBO_END};
combo_t key_combos[COMBO_COUNT] = {
    COMBO(df_combo, MO(_GUI)),
    COMBO(jk_combo, LT(_GUI, KC_ESC)),
};

const key_override_t override_shift_bspc = ko_make_basic(MOD_MASK_SHIFT, KC_BSPC, KC_DEL);

const key_override_t **key_overrides = (const key_override_t *[]){
    &override_shift_bspc,
    NULL,
};

bool process_record_user(uint16_t keycode, keyrecord_t *record) {
#ifdef CONSOLE_ENABLE
    uprintf("KL: kc: 0x%04X, col: %u, row: %u, pressed: %b, time: %u, interrupt: %b, count: %u\n", keycode, record->event.key.col, record->event.key.row, record->event.pressed, record->event.time, record->tap.interrupted, record->tap.count);
#endif
    return true;
}

uint16_t get_tapping_term(uint16_t keycode, keyrecord_t *record) {
    switch (keycode) {
//         case ND_TL2:
// #ifdef CONSOLE_ENABLE
//             uprintf("TAPPING_TERM = 500\n");
// #endif
//             return 500;
        default:
            return 200;
    }
}

bool get_hold_on_other_key_press(uint16_t keycode, keyrecord_t *record) {
    switch (keycode) {
        // ported get_ignore_mod_tap_interrupt
        case QK_MOD_TAP ... QK_MOD_TAP_MAX:
            if (keycode == ND_CTF9) {
                return false;
            } else {
                return true;
            }
        // HOLD_ON_OTHER_KEY_PRESS for the thumb buttons
        case ND_TL1:
        case ND_TL3:
        case ND_TR1:
        case ND_TR2:
        case ND_TR3:
#ifdef CONSOLE_ENABLE
            uprintf("HOLD_ON_OTHER_KEY_PRESS\n");
#endif
            return true;
        default:
            return false;
    }
}

bool get_permissive_hold(uint16_t keycode, keyrecord_t *record) {
    switch (keycode) {
        // PERMISSIVE_HOLD for the space
        case ND_TL2:
#ifdef CONSOLE_ENABLE
            uprintf("PERMISSIVE_HOLD\n");
#endif
            return true;
        default:
            return false;
    }
}

void keyboard_post_init_user(void) {
    //debug_enable = true;
}
