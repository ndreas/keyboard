.ONESHELL:

update-qmk:
	cd qmk_firmware
	git pull
	make git-submodule

sync-files:
	rsync -av ./sofle/ ./qmk_firmware/keyboards/sofle/keymaps/ndreas
	rsync -av ./kimiko/ ./qmk_firmware/keyboards/keycapsss/kimiko/keymaps/ndreas
	rsync -av ./cantor/ ./qmk_firmware/keyboards/cantor/keymaps/ndreas

sofle-elite-c: sync-files
	cd qmk_firmware
	./util/docker_build.sh sofle/rev1:ndreas:dfu

sofle-pro-micro: sync-files
	cd qmk_firmware
	./util/docker_build.sh sofle/rev1:ndreas:avrdude

cantor: sync-files
	cd qmk_firmware
	./util/docker_build.sh cantor:ndreas:flash
